
project(lodepng)
cmake_minimum_required(VERSION 2.8)

#file(COPY build/conaninfo.txt DESTINATION "${CMAKE_BINARY_DIR}")
#include(build/conanbuildinfo.cmake)
include(conanbuildinfo.cmake)
conan_basic_setup()

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES
        lodepng/lodepng.cpp
        lodepng/lodepng_util.cpp
)

add_library(lodepng ${SOURCE_FILES})

target_link_libraries(lodepng ${CONAN_LIBS})



