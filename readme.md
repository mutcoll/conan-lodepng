# conan-lodepng

This is a conan package for [LodePng](http://lodev.org/lodepng/).

It's not tied to any specific version, but to the master branch in github, so don't rely on this for
serious tasks. That's why this package is on "testing" and not "stable".
