from conans import ConanFile, CMake

class MyProjectWithConan(ConanFile):
    name = "LodePNG"
    author = "Lode Vandevenne"
    version = "master"
    exports = "CMakeLists.txt"
    settings = "os", "compiler", "build_type", "arch"
    url = "http://lodev.org/lodepng/"
    license = "zlib"
    description = "LodePNG is a PNG image decoder and encoder, all in one, no dependency or linkage to zlib or libpng required. It's made for C (ISO C90), and has a C++ wrapper with a more convenient interface on top. "
    # requires = ""
    generators = "cmake"
    options = {"shared": [True, False]} # Values can be True or False (number or string value is also possible)
    default_options = "shared=False" # Default value for shared is False (static)


    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()


    def package(self):
        self.copy("*.h", dst="include", src="lodepng")
        self.copy("*.a", dst="lib", src="lib")

    def package_info(self):
        self.cpp_info.libs = ["lodepng"]


    def source(self):
        self.run("git clone https://github.com/lvandeve/lodepng.git")
